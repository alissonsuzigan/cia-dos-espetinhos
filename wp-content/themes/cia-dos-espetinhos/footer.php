<?php
/**
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

  </div>
  <footer id="footer" class="site-footer" role="contentinfo">
    <div class="content-width">

      <div class="opening-hours">
        <p class="opening-hours-title">Horário de Funcionamento:</p>
        <p class="opening-hours-content">Todos os dias das 17:30 às 00:00</p>
      </div>

      <div class="special-events">
        <a href="mailto:contato@ciadosespetinhos.com.br" class="special-events-link">Pacotes Especiais para Eventos</a>
      </div>

      <div class="social-media">
        <a target="_blank" href="https://www.facebook.com/CiaDosEspetinhosGuaratinguetasp" class="social-link"><i class="icon-facebook"></i></a>
        <a target="_blank" href="#" class="social-link"><i class="icon-twitter"></i></a>
        <a target="_blank" href="#" class="social-link"><i class="icon-instagran"></i></a>
      </div>

    </div>
	</footer>
</div>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/vendor/jquery/jquery-2.0.3.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/global.min.js"></script>
<?php if (is_home()) { ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/carousel.min.js"></script>
<?php } ?>
<?php if (is_page('lojas')) { ?>
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/storeLocator.min.js"></script>
<?php } ?>
</body>
</html>
