<div id="banner" class="site-banner">
  <div class="banner-content">
    <div class="banner-area">
      <?php
        if ( is_page() ) {
          if ( is_front_page() ) {
            $list = CFS()->get("banner-list");
            foreach ($list as $item) {
              $image = $item["banner-image"];
              $link = $item["banner-link"];
              
              if ($link) {echo '<a href="'. $link .'">';}
                echo '<img src="'. $image .'">';
              if ($link) {echo '</a>';}
            }

          } else {
            the_post_thumbnail( "full" );
          }

        } else {
          echo wp_get_attachment_image( 33, 'full' );
        }
      ?>
    </div>
  </div>
</div>