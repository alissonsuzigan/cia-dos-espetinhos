<?php
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<!-- HOME SESSION -->
<div class="home-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">

      <!-- user content - added by admin -->
      <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

          <header class="main-header">
            <?php the_title( '<h1 class="main-title">', '</h1>' ); ?>
          </header>
          <div class="main-content">
            <?php the_content(); ?>
          </div>

        </article>
      <?php endwhile; ?>

    </main>
  </div>
</div>


<!-- NEWS SESSION -->
<div class="news-session">
  <div class="content-width">
    <div class="title-news">Novidades</div>
    <main id="main" class="site-main" role="main">

      <!-- user content - added by admin -->
      <?php
        query_posts("post_type=post");
        while ( have_posts() ) : the_post();
        $posClass = ($wp_query->current_post %2 == 0) ? "right-pos" : "left-pos";
      ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( $posClass ); ?>>
        <div class="container-info">
          <header class="post-header">
            <?php
              echo '<time class="post-date">';
              the_time('F');
              echo '<span class="date-separator">•</span>';
              the_time('d');
              echo '<span class="date-separator">•</span>';
              the_time('Y');
              echo '</time>';
              the_title( sprintf( '<h2 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
            ?>
          </header>
          <p class="content-separator">*</p>
          <div class="post-content">
            <?php // the_content(); ?>
            <?php the_excerpt(); ?>
          </div>
          <div class="see-more-content">
            <a class="see-more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Ver mais</a>
          </div>
        </div>

        <div class="container-image">
          <figure class="post-image">
             <?php the_post_thumbnail( array(570, 340) ); ?>
          </figure>
        </div>
      </article>

      <?php endwhile; ?>

    </main>
  </div>
</div>



<!-- CONTACT SESSION -->
<div class="contact-session">
  <div class="content-width contact-container">
    <div class="title-contact">Contato</div>
    <?php get_template_part("contact-form"); ?>
  </div>
</div>


<?php get_footer(); ?>