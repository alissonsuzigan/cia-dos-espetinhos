<form id="contact-form" class="contact-form" action="contact-post.php" method="post">

  <div class="subject-box">
    <select class="select subject required" name="subject-select">
      <option value="-1">Escolha um assunto *</option>
      <option value="0">Dúvidas</option>
      <option value="1">Sugestões</option>
      <option value="2">Franquias</option>
      <option value="3">Eventos</option>
      <option value="4">Outras</option>
    </select>

    <p class="subject-description">
      Selecione o assunto de seu interesse <br>
      e envie-nos uma mensagem <br>
      preenchendo corretamente <br>
      os campos ao lado.<br>
      Retornaremos com uma resposta <br>
      o mais breve possível!
    </p>
    <small>( * ) Campos obrigatórios</small>
  </div>

  <div class="form-box-contact">
    <div class="field-line">
      <input class="input half-field required name" type="text" name="form-name" placeholder="NOME *">
      <input class="input half-field tel phone" type="text" name="form-phone" placeholder="TELEFONE">
    </div>
    <input class="input required email" type="text" name="form-email" placeholder="EMAIL *">
    <textarea class="textarea required message" name="form-message" rows="7" placeholder="MENSAGEM *"></textarea>

    <input class="btn btn-send" type="submit" value="ENVIAR">
  </div>
  
  <div id="info-box" class="info-box">
    <p class="info-label">Preencha os campos e envie o formulário</p>
  </div>
</form>