/*
  MAP VIEWS - google.maps.MapTypeId.[MAP_TYPE]

  - ROADMAP (normal, default 2D map)
  - SATELLITE (photographic map)
  - HYBRID (photographic map + roads and city names)
  - TERRAIN (map with mountains, rivers, etc.)
*/

app.utils.googleMaps = (function() {
  "use strict";

  var map, marker,
      ZOOM = 17,
      MAP_TYPE = google.maps.MapTypeId.ROADMAP;


  function updateMap(lat, lng, zoom, type) {
    var mapProp = {
          center         : new google.maps.LatLng(lat, lng),
          zoom           : zoom || ZOOM,
          MapTypeId      : type || MAP_TYPE
        };

    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    marker = new google.maps.Marker({
      map: map,
      position: mapProp.center,
      animation: google.maps.Animation.DROP
    });
  }

  return {
    updateMap : updateMap
  };
})();