app.utils.pushState = (function() {
  "use strict";


  function goTo(url) {
    // console.log(url);
    if (typeof history.pushState !== "undefined") {
      history.pushState(null, null, url);
      return false;
    }
  }


  function bindEvents() {
    window.onpopstate = function() {
      window.location.href = window.location.href;
    };
  }

  function update (newId) {
    // console.log("newId " + newId);
    goTo(app.menuData[newId].url);
  }

  return {
    init: function() {
      bindEvents();
    },
    update : update
  };
})();
app.utils.pushState.init();