app.navigation = (function() {
  "use strict";

  var component = $("#navigation"),
      items = component.find(".nav-item"),
      icons = component.find(".icon");


  function navClick(target) {
    var newId,
        pageId = app.pageId;

    if (target.hasClass("nav-left")) {
      newId = ((pageId -1) < 0) ? app.menuData.length-1 : pageId -1;
      app.slide.goLeft(newId);
    }

    if (target.hasClass("nav-right")) {
      newId = ((pageId +1) === app.menuData.length) ? 0 : pageId +1;
      app.slide.goRight(newId);
    }

    if (target.hasClass("nav-down")) {
      newId = -1;
      app.slide.goDown(newId);
      return;
    }

    app.updateId(newId);
    // console.log(newId);
  }

  function changeColor(newId) {
    icons.css({"color" : app.menuData[newId].mainColor });
  }

  function bindEvents() {
    items.on("click", function(e) {
      e.preventDefault();
      if (app.isBlock){return;}
      navClick($(this));
    });
  }

  function update (newId) {
    // console.log("navigation update " + newId);
    changeColor(newId);
  }

  return {
    init: function() {
      bindEvents();
      // changeColor(app.pageId);
    },
    update : update
  };
})();
app.navigation.init();