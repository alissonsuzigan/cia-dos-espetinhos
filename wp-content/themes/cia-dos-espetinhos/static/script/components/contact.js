app.contact = (function() {
  "use strict";

  var componet = $("#contact-form"),
      required = componet.find(".required"),
      infoBox =  componet.find(".info-box"),
      timeoutBox;


  function validate(e) {
    e.preventDefault();

    var inputTemp,
        hasError = false;

    required.each(function(idx) {
      inputTemp = required.eq(idx);

      if($.trim(inputTemp.val()) === "") {
        hasError = true;
        inputTemp.addClass("error");
        // infoBox.addClass("error");
      }
    });

    if ($(".subject option:selected").val() === "-1") {
      hasError = true;
      $(".subject").addClass("error");
    };

    if (hasError) {
      // console.log("error");
      showInfo("Preencha os campos obrigatórios antes de enviar.", "error");
      return false;
    } else {
      return true;
    };
  }


  function sendForm() {
    var data = {
      formSubject: $(".subject option:selected").text(),
      formName: $(".name").val(),
      formPhone: $(".phone").val(),
      formEmail: $(".email").val(),
      formMessage: $(".message").val()
    }

    $.ajax({
      type: "POST",
      // url: "http://localhost/cia-dos-espetinhos/wp-content/themes/cia-dos-espetinhos/contact-post.php", 
      url: "http://www.ciadosespetinhos.com.br/v2/wp-content/themes/cia-dos-espetinhos/contact-post.php", 
      async: true,
      data: data,
      success: function(data) {
        // console.log("success");

        if (data === "success") {
          showInfo("Mensagem enviada com sucesso!", "success");
        } else {
          showInfo("Erro no envio da mensagem. Tente outra vez.", "error"); 
        };

        setTimeout(function() {
          $(".name").val("");
          $(".phone").val("");
          $(".email").val("");
          $(".message").val("");
        }, 5000);
      },
      beforeSend: function() {
        showInfo("Eviando mensagem...", "");
      },
      complete: function() {
        // hideInfo();
      },
      error: function() {
        // console.log("error");
        showInfo("Erro no envio da mensagem. Tente outra vez.", "error");
      }
    });
  }


  function showInfo(message, status, timer) {
    timer = timer || 4000;
    infoBox.find(".info-label").text(message);
    
    if (status) {
      infoBox.addClass(status);
    };

    setTimeout(function() {
      infoBox.addClass("box-in");
    }, 300);


    clearTimeout(timeoutBox);
    timeoutBox = setTimeout(function() {
      infoBox.removeClass("box-in");

      timeoutBox = setTimeout(function() {
        infoBox.attr("class", "info-box");
      }, 1000);
    }, timer);
  }


 function onInputFocus () {
    var target = $(this);
    if (target.hasClass("error")) {
      target.removeClass("error");
    };
  }


  function bindEvents () {
    // componet.submit(validate);
    componet.on("submit", function(e) {
      if (validate(e)) {
        sendForm();
      };
    });
    required.focus(onInputFocus);
  }


  return {
    init: function() {
      bindEvents();
    },
  };
})();
app.contact.init();