app.slide = (function() {
  "use strict";

  var area = $("#area"),
      minTime = 100,
      maxTime = 1000;

  function goUp () {
    console.log("goTop");

    area.css("top", "-100%").removeClass("hide");
    setTimeout(function(){
      area.addClass("anima").css("top", "0%");
    }, minTime);
    setTimeout(function(){
      area.removeClass("anima");
    }, maxTime);
  }

  function goDown () {
    console.log("goBottom");

    area.css("top", "0%");
    setTimeout(function(){
      area.addClass("anima").css("top", "100%");
    }, minTime);
    setTimeout(function(){
      area.removeClass("anima");
    }, maxTime);

  }


  function goLeft (newId) {
    var pageIn  = area.find(".item-" + newId),
        pageOut = area.find(".item-" + app.pageId);

    pageOut.css("left", "0%");
    pageIn.css("left", "-100%").removeClass("hide");
    setTimeout(function(){
      pageOut.addClass("anima").css("left", "100%");
      pageIn.addClass("anima").css("left", "0%");
    }, minTime);
    setTimeout(function(){
      pageOut.removeClass("anima").addClass("hide");
      pageIn.removeClass("anima");
    }, maxTime);
  }


  function goRight (newId) {
    var pageIn  = area.find(".item-" + newId),
        pageOut = area.find(".item-" + app.pageId);

    pageOut.css("left", "0%");
    pageIn.css("left", "100%").removeClass("hide");
    setTimeout(function(){
      pageOut.addClass("anima").css("left", "-100%");
      pageIn.addClass("anima").css("left", "0%");
    }, minTime);
    setTimeout(function(){
      pageOut.removeClass("anima").addClass("hide");
      pageIn.removeClass("anima");
    }, maxTime);
  }


  return {
    // init: function() { }
    goUp     : goUp,
    goDown   : goDown,
    goLeft   : goLeft,
    goRight  : goRight

  };
})();
// app.slide.init();