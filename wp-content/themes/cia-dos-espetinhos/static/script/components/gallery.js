app.menu = (function() {
  "use strict";


  function openImage(urlImage) {
    var galleryHtml = "";

    galleryHtml += "<div class='fullImageContainer'>";
    galleryHtml +=   "<img src='" + urlImage + "'>";
    galleryHtml +=   "<div class='fullImageBg'></div>";
    galleryHtml += "</div>";


    $("body").prepend(galleryHtml).css("overflow", "hidden");

    $(".fullImageContainer").on("click", function() {
      // e.preventDefault();
      console.log("fullImageContainer");
      closeImage();
    });
  }


  function closeImage() {
    $(".fullImageContainer").remove();
    $("body").css("overflow", "scroll");
  }


  function bindEvents() {
    $(".gallery").find("a").on("click", function(e) {
      e.preventDefault();
      // console.log($(this).attr("href"));
      openImage($(this).attr("href"));
    });

    
  }

  



  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.menu.init();