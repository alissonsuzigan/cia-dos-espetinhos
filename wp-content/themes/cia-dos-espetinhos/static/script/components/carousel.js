app.carousel = (function() {
  "use strict";


  function bindEvents() {
    $("#banner").find(".banner-area").owlCarousel({
      singleItem:true,
      pagination : false,
      navigation : true,
      slideSpeed : 600,
      paginationSpeed : 600,
      rewindSpeed : 800,
      navigationText : ["",""]
    });
  }

  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.carousel.init();