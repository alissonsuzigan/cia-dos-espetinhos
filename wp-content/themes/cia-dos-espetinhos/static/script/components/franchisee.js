app.franchisee = (function() {
  "use strict";

  var componet = $("#franchisee-form"),
      inputs =   componet.find(".input"),
      required = componet.find(".required"),
      infoBox =  componet.find(".info-box"),
      timeoutBox;


  function validate(e) {
    e.preventDefault();

    var inputTemp,
        hasError = false;

    required.each(function(idx) {
      inputTemp = required.eq(idx);

      if($.trim(inputTemp.val()) === "") {
        hasError = true;
        inputTemp.addClass("error");
        // infoBox.addClass("error");
      }
    });

    if (hasError) {
      // console.log("error");
      showInfo("Preencha os campos obrigatórios antes de enviar.", "error");
      return false;
    } else {
      return true;
    };
  }


  function sendForm() {
    var data = {
      form_nome:              componet.find("[name='form-nome']").val(),
      form_idade:             componet.find("[name='form-idade']").val(),
      form_email:             componet.find("[name='form-email']").val(),
      form_cpf:               componet.find("[name='form-cpf']").val(),
      form_celular:           componet.find("[name='form-celular']").val(),
      form_rg:                componet.find("[name='form-rg']").val(),
      form_telefone_fixo:     componet.find("[name='form-telefone-fixo']").val(),
      form_sexo:              componet.find("[name='form-sexo']").val(),
      form_endereco:          componet.find("[name='form-endereco']").val(),
      form_cep:               componet.find("[name='form-cep']").val(),
      form_cidade:            componet.find("[name='form-cidade']").val(),
      form_estado:            componet.find("[name='form-estado']").val(),
      form_interesse_cidade:  componet.find("[name='form-interesse-cidade']").val(),
      form_interesse_estado:  componet.find("[name='form-interesse-estado']").val(),
      form_invest_valor:      componet.find("[name='form-invest-valor']").val(),
      form_como_soube:        componet.find(".how-know option:selected").text()
    }

    console.log(data);

    $.ajax({
      type: "POST",
      // url: "http://localhost/cia-dos-espetinhos/wp-content/themes/cia-dos-espetinhos/franchisee-post.php", 
      url: "http://www.ciadosespetinhos.com.br/v2/wp-content/themes/cia-dos-espetinhos/franchisee-post.php", 
      async: true,
      data: data,
      success: function(data) {
        // console.log("success");

        if (data === "success") {
          showInfo("Mensagem enviada com sucesso!", "success");
        } else {
          showInfo("Erro no envio da mensagem. Tente outra vez.", "error"); 
        };

        setTimeout(function() {
            componet.find("[name='form-nome']").val("");
            componet.find("[name='form-idade']").val("");
            componet.find("[name='form-email']").val("");
            componet.find("[name='form-cpf']").val("");
            componet.find("[name='form-celular']").val("");
            componet.find("[name='form-rg']").val("");
            componet.find("[name='form-telefone-fixo']").val("");
            componet.find("[name='form-sexo']").val("");
            componet.find("[name='form-endereco']").val("");
            componet.find("[name='form-cep']").val("");
            componet.find("[name='form-cidade']").val("");
            componet.find("[name='form-estado']").val("");
            componet.find("[name='form-interesse-cidade']").val("");
            componet.find("[name='form-interesse-estado']").val("");
            componet.find("[name='form-invest-valor']").val("");
            componet.find("[name='form-como-soube']").val("");
        }, 5000);
      },
      beforeSend: function() {
        showInfo("Eviando mensagem...", "");
      },
      complete: function() {
        // hideInfo();
      },
      error: function() {
        // console.log("error");
        showInfo("Erro no envio da mensagem. Tente outra vez.", "error");
      }
    });
  }


  function showInfo(message, status, timer) {
    timer = timer || 4000;
    infoBox.find(".info-label").text(message);
    
    if (status) {
      infoBox.addClass(status);
    };

    setTimeout(function() {
      infoBox.addClass("box-in");
    }, 300);


    clearTimeout(timeoutBox);
    timeoutBox = setTimeout(function() {
      infoBox.removeClass("box-in");

      timeoutBox = setTimeout(function() {
        infoBox.attr("class", "info-box");
      }, 1000);
    }, timer);
  }


 function onInputFocus () {
    var target = $(this);
    if (target.hasClass("error")) {
      target.removeClass("error");
    };
  }


  function bindEvents () {
    componet.on("submit", function(e) {
      if (validate(e)) {
        sendForm();
      };
    });
    required.focus(onInputFocus);
  }


  return {
    init: function() {
      bindEvents();
    },
  };
})();
app.franchisee.init();