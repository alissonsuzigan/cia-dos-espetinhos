app.storeLocator = (function() {
  "use strict";

  var storeArr, dataTemplate,
      // urlData = "http://www.ciadosespetinhos.com.br/v2/wp-content/themes/cia-dos-espetinhos/static/script/store-locator.json";
      urlData = "/cia-dos-espetinhos/wp-content/themes/cia-dos-espetinhos/static/script/store-locator.json";


  function loadStoreData(url) {
    $.ajax({
      type: "get",
      dataType: "json",
      url: url,
      success: function(data) {
        storeArr = data.store;
        buildStoreSelector();
        updateStoreMap(0);
        updateStoreInfo(0);
        $("#main").find(".store-select").removeAttr("disabled");
      },

      error: function() {
        console.log("load store locator error!");
      }
    });
  }

  function buildStoreSelector() {
    var items = "";
    for (var i = 0; i < storeArr.length; i++) {
      items += " <option value='" + i + "'>" + storeArr[i]["city"] + " - " + storeArr[i]["uf"] + "</option>";
    };
    $("#store-form").find(".store-select").append(items);
  }

  function updateStoreMap(id) {
    var lat = storeArr[id]["lat"],
        lng = storeArr[id]["lng"];
    app.utils.googleMaps.updateMap(lat, lng);
  }

  function updateStoreInfo(id) {
    dataTemplate  = "<p class='title'>[[city]] - [[uf]]</p>";
    dataTemplate += "<p class='text'>[[address]]</p>";
    dataTemplate += "<p class='text'>[[phone]]</p>";
    dataTemplate += "<p class='text'>[[email]]</p>";
    dataTemplate += "<p class='text'>[[moreInfo]]</p>";

    var html = dataTemplate
        .replace("[[city]]", storeArr[id]["city"] || "")
        .replace("[[uf]]", storeArr[id]["uf"] || "")
        .replace("[[address]]", storeArr[id]["address"] || "")
        .replace("[[phone]]", storeArr[id]["phone"] || "")
        .replace("[[email]]", storeArr[id]["email"] || "")
        .replace("[[moreInfo]]", storeArr[id]["moreInfo"] || "");

    $("#store-address").html(html);
  }


  function bindEvents() {
    $("#main").find(".store-select").on("change", function() {
      updateStoreMap($(this).val());
      updateStoreInfo($(this).val())
    })
  }

  return {
    init: function() {
      loadStoreData(urlData);
      bindEvents();
    }
  };
})();
app.storeLocator.init();
