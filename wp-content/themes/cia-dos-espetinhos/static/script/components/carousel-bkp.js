app.carousel = (function() {
  "use strict";

  var component   = $("#banner"),
      content     = component.find(".banner-content"),
      items       = component.find(".banner-item"),

      minTime     = 50,
      slideTime   = 700,
      canSlide    = true,
      timeChange  = 3000,
      idx         = 0,
      itemsLength = items.length,
      timeout;


  
  function autoPlay() {
    timeout = setTimeout(function(){
      var newId = (idx+1 === itemsLength) ? 0 : idx+1;
      changeSlide(newId);
    }, timeChange);
  }


  function changeSlide(newId) {
    console.log("changeSlide");

    if (idx === newId) {return;}
    if (!canSlide) {return;}

    var itemIn  = items.eq(newId),
        itemOut = items.eq(idx);

    canSlide = false;
    itemOut.css("z-index", "1");
    itemIn.css({"z-index": "2", "opacity": "0"});

    setTimeout(function(){
      itemIn.animate({"opacity": 1}, slideTime, function() {
        itemOut.css("opacity", "0");
        canSlide = true;
      });
      idx = newId;
    }, minTime);

    clearTimeout(timeout);
    autoPlay();

  }


  function buildEvents() {
    // content.append(control);

    content.on("click", ".carousel-prev", function(e) {
      console.log(e.target);
      var newId = (idx-1 < 0) ? itemsLength-1 : idx-1;
      changeSlide(newId);
    });

    content.on("click", ".carousel-next", function(e) {
      console.log(e.target);
      var newId = (idx+1 === itemsLength) ? 0 : idx+1;
      changeSlide(newId);
    });
   
  }


  return {
    init: function() {
      items.css({"opacity": "0"});
      items.eq(0).css({"opacity": "1"});

      buildEvents();
    }
  };
})();
app.carousel.init();