app.menu = (function() {
  "use strict";

  var component = $("#menu"),
      links = component.find(".menu-link");




  function bindEvents() {
    links.on("click", function(e) {
      e.preventDefault();
      console.log(e.target);
    });
  }


  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.menu.init();