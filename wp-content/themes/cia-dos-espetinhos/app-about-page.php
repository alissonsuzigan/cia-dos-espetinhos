<?php
/**
 * Template Name: Sobre
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="about-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">

      <!-- user content - added by admin -->
      <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

          <header class="main-header">
            <?php the_title( '<h1 class="main-title">', '</h1>' ); ?>
          </header>
          <div class="main-content">
            <?php the_content(); ?>
          </div>

        </article>
      <?php endwhile; ?>

    </main>
  </div>
</div>

<?php get_footer(); ?>


