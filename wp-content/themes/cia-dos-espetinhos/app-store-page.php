<?php
/**
 * Template Name: Loja
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();

// $json = file_get_contents("http://www.ciadosespetinhos.com.br/store-locator.json");
// $json = file_get_contents("http://localhost/cia-dos-espetinhos/wp-content/themes/cia-dos-espetinhos/static/script/store-locator.json");
// $json = file_get_contents("http://www.ciadosespetinhos.com.br/v2/wp-content/themes/cia-dos-espetinhos/static/script/store-locator.json");
// $data = json_decode($json, true);
?>

<div class="store-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">

      <form id="store-form" class="store-form">
        <div class="store-selector">
          <select class="select store-select" name="subject-select" disabled>
            <!-- <option value="">Estado</option> -->
            <?php
              // foreach ($data["store"] as $key => $value) {
              //   echo('<option value="'. $key .'">'. $value["city"] .' - ' . $value["uf"] .'</option>');
              // } ?>
          </select>
        </div>

        <div id="store-address" class="store-address"></div>
      </form>


      <div class="store-map">
        <div id="googleMap" class="google-map"></div>
      </div>

    </main>
  </div>
</div>

<?php get_footer(); ?>