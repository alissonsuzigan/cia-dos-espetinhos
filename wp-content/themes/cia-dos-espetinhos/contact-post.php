<?php 

$subject = $name = $phone = $email = $message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $subject =    stripslashes(trim($_POST["formSubject"]));
  $name =       stripslashes(trim($_POST["formName"]));
  $phone =      stripslashes(trim($_POST["formPhone"]));
  $email =      stripslashes(trim($_POST["formEmail"]));
  $message =    stripslashes(trim($_POST["formMessage"]));
  $pattern  =   "/[\r\n]|Content-Type:|Bcc:|Cc:/i";

  // if (preg_match($pattern, $name) || preg_match($pattern, $email) || preg_match($pattern, $message)) {
  //     die("Header injection detected");
  // }

  if($name && $phone && $email) {
    $body =  "<p style='font-size:14px;'>";
    $body .= "<strong>Dados enviados pelo site Cia. dos Espetinhos</strong><br><br>";
    $body .= "<strong>Assunto: </strong>    $subject <br>";
    $body .= "<strong>Nome: </strong>       $name <br>";
    $body .= "<strong>Telefone: </strong>   $phone <br>";
    $body .= "<strong>e-mail: </strong>     $email <br>";
    $body .= "<strong>message: </strong>    $message <br>";
    $body .= "</p>";

    // headers
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: Cia. dos Espetinhos | Contato <contato@ciadosespetinhos.com.br>\r\n";
    $headers .= "Return-Path: contato@ciadosespetinhos.com.br\r\n";

    // CONFIG INFO =============================================
    $emailSubject = "Formulário de contato - Cia. dos Espetinhos";
    // $emailTo = "@gmail.com";
    $emailTo = "contato@ciadosespetinhos.com.br";

    // if ($subject == "Franquias") {
    //   $emailTo = "ciadosespetinhos@saffi.com.br";
    // }

    if (mail($emailTo, $emailSubject, $body, $headers)) {
      echo "success";
      return true;
    }

  } else {
    echo "error";
  }

}
?>