<?php
/**
 * Template Name: Contato
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="contact-session">
  <main id="main" class="site-main" role="main">
    <div class="content-width contact-container">
      <?php get_template_part("contact-form"); ?>
    </div>
  </main>
</div>

<?php get_footer(); ?>


