<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $nome =            stripslashes(trim($_POST["form_nome"]));
  $idade =           stripslashes(trim($_POST["form_idade"]));
  $email =           stripslashes(trim($_POST["form_email"]));
  $cpf =             stripslashes(trim($_POST["form_cpf"]));
  $celular =         stripslashes(trim($_POST["form_celular"]));
  $rg =              stripslashes(trim($_POST["form_rg"]));
  $telefoneFixo =    stripslashes(trim($_POST["form_telefone_fixo"]));
  $sexo =            stripslashes(trim($_POST["form_sexo"]));
  $endereco =        stripslashes(trim($_POST["form_endereco"]));
  $cep =             stripslashes(trim($_POST["form_cep"]));
  $cidade =          stripslashes(trim($_POST["form_cidade"]));
  $estado =          stripslashes(trim($_POST["form_estado"]));
  $cidadeInteresse = stripslashes(trim($_POST["form_interesse_cidade"]));
  $estadoInteresse = stripslashes(trim($_POST["form_interesse_estado"]));
  $valorInvest =     stripslashes(trim($_POST["form_invest_valor"]));
  $comoSoube =       stripslashes(trim($_POST["form_como_soube"]));
  $pattern  =        "/[\r\n]|Content-Type:|Bcc:|Cc:/i";


  if($nome && $email) {
    $body =  "<p style='font-size:14px;'>";
    $body .= "<strong>Dados enviados pelo site Cia. dos Espetinhos</strong><br><br>";

    $body .= "<strong>Nome: </strong> $nome <br>";
    $body .= "<strong>Idade: </strong> $idade <br>";
    $body .= "<strong>Email: </strong> $email <br>";
    $body .= "<strong>CPF: </strong> $cpf <br>";
    $body .= "<strong>Celular: </strong> $celular <br>";
    $body .= "<strong>RG: </strong> $rg <br>";
    $body .= "<strong>Telefone Fixo: </strong> $telefoneFixo <br>";
    $body .= "<strong>Sexo: </strong> $sexo <br>";
    $body .= "<strong>Endereco: </strong> $endereco <br>";
    $body .= "<strong>CEP: </strong> $cep <br>";
    $body .= "<strong>Cidade: </strong> $cidade <br>";
    $body .= "<strong>Estado: </strong> $estado <br>";
    $body .= "<strong>Cidade de interesse: </strong> $cidadeInteresse <br>";
    $body .= "<strong>Estado de interesse: </strong> $estadoInteresse <br>";
    $body .= "<strong>valorInvest: </strong> $valorInvest <br>";
    $body .= "<strong>comoSoube: </strong> $comoSoube <br>";
    $body .= "</p>";

    // headers
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: Cia. dos Espetinhos | Contato <contato@ciadosespetinhos.com.br>\r\n";
    $headers .= "Return-Path: contato@ciadosespetinhos.com.br\r\n";

    // CONFIG INFO =============================================
    $emailSubject = "Formulário de Franquiados - Cia. dos Espetinhos";
    // $emailTo = "contato@ciadosespetinhos.com.br";
    $emailTo = "ciadosespetinhos@saffi.com.br";

    if (mail($emailTo, $emailSubject, $body, $headers)) {
      echo "success";
      return true;
    }

  } else {
    echo "error";
  }

}
?>