<?php
/**
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

  get_header(); 
  get_template_part( "news-filter" );
  get_template_part( "content-news" );
  get_footer();
?>