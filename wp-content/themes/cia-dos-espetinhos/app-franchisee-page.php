<?php
/**
 * Template Name: Franquias
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="franchisee-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">

      <!-- system content - added by code -->
      <div class="franchisee-container">
        <form id="franchisee-form" class="franchisee-form" action="franchisee-post.php" method="post">

          <div class="franchisee-description-box">
            <h1 class="title orange">Seja um Franqueado</h1>
            <p class="text-description">
              Se você tem um perfil dinâmico, disponibilidade de dedicação e gosta de gerir um negócio que faz parte dos momentos de diversão das pessoas, preencha o cadastro abaixo e aguarde nosso contato.
            </p>
          </div>

          <div class="form-box form-box-franchisee">
            <div class="field-line">
              <input class="input half-big required" type="text" name="form-nome" placeholder="NOME">
              <input class="input half-small" type="text" name="form-idade" placeholder="IDADE">
            </div>
            <div class="field-line">
              <input class="input half-big required" type="text" name="form-email" placeholder="EMAIL">
              <input class="input half-small" type="text" name="form-cpf" placeholder="CPF">
            </div>
            <div class="field-line">
              <input class="input half-big" type="text" name="form-celular" placeholder="CELULAR">
              <input class="input half-small" type="text" name="form-rg" placeholder="RG">
            </div>
            <div class="field-line">
              <input class="input half-big" type="text" name="form-telefone-fixo" placeholder="TELEFONE FIXO">
              <input class="input half-small" type="text" name="form-sexo" placeholder="SEXO">
            </div>
            <div class="field-line">
              <input class="input half-big" type="text" name="form-endereco" placeholder="ENDEREÇO">
              <input class="input half-small" type="text" name="form-cep" placeholder="CEP">
            </div>
            <div class="field-line">
              <input class="input half-big" type="text" name="form-cidade" placeholder="CIDADE">
              <input class="input half-small" type="text" name="form-estado" placeholder="ESTADO">
            </div>
          </div>

          <p class="franquia-text-up">região de interesse da franquia:</p>
          <div class="field-line">
            <input class="input half-big" type="text" name="form-interesse-cidade" placeholder="CIDADE">
            <input class="input half-small" type="text" name="form-interesse-estado" placeholder="ESTADO">
          </div>

          <p class="franquia-text-up">capacidade financeira para investimento</p>
          <div class="field-line">
            <input class="input" type="text" name="form-invest-valor" placeholder="R$">
          </div>

          <p class="franquia-text-up">como conheceu a cia dos espetinhos?</p>
          <select class="select how-know" name="form-como-soube">
            <option value="">Indicação</option>
            <option value="">Site</option>
            <option value="">Google</option>
            <option value="">É cliente</option>
            <option value="">Revista</option>
            <option value="">Jornal</option>
            <option value="">Facebook</option>
            <option value="">Outros</option>
          </select>
          <input class="btn btn-send" type="submit" value="ENVIAR">
          
          <div id="info-box" class="info-box">
            <p class="info-label">Preencha os campos e envie o formulário</p>
          </div>
        </form>
        <p class="obs">
          <span class="orange">Obs: </span>Neste momento, a expansão da Cia dos Espetinhos é para o interior de São Paulo.
        </p>
      </div>

    </main>
  </div>
</div>

<?php get_footer(); ?>


