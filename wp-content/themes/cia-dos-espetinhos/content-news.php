<div class="news-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">
      <!-- user content - added by admin -->

      <?php 
      if ( have_posts() ) : ?>

        <header class="page-header">
          <?php
            the_archive_title( '<h1 class="page-title">', '</h1>' );
            // the_archive_description( '<div class="taxonomy-description">', '</div>' );
          ?>
        </header><!-- .page-header -->
        <?php
        while ( have_posts() ) : the_post();
          // get_template_part( 'content', get_post_format() );
          $posClass = ($wp_query->current_post %2 == 0) ? "right-pos" : "left-pos";
        ?>
        
        <article id="post-<?php the_ID(); ?>" <?php post_class( $posClass ); ?>>

          <div class="container-info">
            <header class="post-header">
              <?php
                echo '<time class="post-date">';
                the_time('F');
                echo '<span class="date-separator">•</span>';
                the_time('d');
                echo '<span class="date-separator">•</span>';
                the_time('Y');
                echo '</time>';

                the_title( sprintf( '<h2 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
              ?>
            </header>

            <p class="content-separator">*</p>

            <div class="post-content">
              <?php the_excerpt(); ?>
            </div>
            <div class="see-more-content">
              <a class="see-more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Ver mais</a>
            </div>
          </div>

          <div class="container-image">
            <figure class="post-image">
               <?php the_post_thumbnail( array(570, 340) ); ?>
            </figure>
          </div>

        </article>

        <?php 
        endwhile;
        // Previous/next page navigation.
        the_posts_pagination( array(
          'prev_text' => "• Mais recentes",
          'next_text' => "Mais antigos •"
        ) );

      else :
        get_template_part( 'content', 'none' );
      endif;
      ?>

    </main>
  </div>
</div>
