<?php
/**
 * Template Name: Novidades
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<!-- news bar -->
<div class="news-filter-session">
  <div class="content-width">
    <form class="news-filter-form">

      <label class="filter-label">Filtrar por:</label>
      <select class="select" name="event-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
        <option value="">Assunto</option>
        <?php
          $categories = get_categories('orderby=name');
          foreach ($categories as $category) {
            $category_link = get_term_link( $category );
            $option = '<option value="' . $category_link .'">';
            $option .= $category->cat_name;
            $option .= '</option>';
            echo $option;
          }
        ?>
      </select>

      <select class="select" name="month" onchange='document.location.href=this.options[this.selectedIndex].value;'>
        <option value="">Data</option>
        <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12, 'format' => 'option' ) ); ?>
      </select>

    </form>
  </div>
</div>



<div class="news-session">
  <div class="content-width">
    <main id="main" class="site-main" role="main">
      <!-- user content - added by admin -->
      <?php
        query_posts("post_type=post");
        while ( have_posts() ) : the_post();
        $posClass = ($wp_query->current_post %2 == 0) ? "right-pos" : "left-pos";
      ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( $posClass ); ?>>

        <div class="container-info">
          <header class="post-header">
            <?php
              echo '<time class="post-date">';
              the_time('F');
              echo '<span class="date-separator">•</span>';
              the_time('d');
              echo '<span class="date-separator">•</span>';
              the_time('Y');
              echo '</time>';
              the_title( sprintf( '<h2 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
            ?>
          </header>

          <p class="content-separator">*</p>

          <div class="post-content">
            <?php // the_content(); ?>
            <?php the_excerpt(); ?>
          </div>
          <div class="see-more-content">
            <a class="see-more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Ver mais</a>
          </div>
        </div>

        <div class="container-image">
          <figure class="post-image">
             <?php the_post_thumbnail( array(570, 340) ); ?>
          </figure>
        </div>
      </article>

      <?php
        endwhile;

        // Previous/next page navigation.
        the_posts_pagination( array(
          'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
          'next_text'          => __( 'Next page', 'twentyfifteen' ),
          'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
        ));
      ?>

    </main>
  </div>
</div>

<?php get_footer(); ?>