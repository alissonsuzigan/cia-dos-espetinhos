<div class="news-filter-session">
  <div class="content-width">
    <form class="news-filter-form">

      <label class="filter-label">Filtrar por:</label>
      <select class="select" name="event-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
        <option value="">Assunto</option>
        <?php
        $categories = get_categories('orderby=name');
        foreach ($categories as $category) {
          $category_link = get_term_link( $category );
          $option = '<option value="' . $category_link .'">';
          $option .= $category->cat_name;
          $option .= '</option>';
          echo $option;
        } ?>
      </select>

      <select class="select" name="month" onchange='document.location.href=this.options[this.selectedIndex].value;'>
        <option value="">Data</option>
        <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12, 'format' => 'option' ) ); ?>
      </select>

    </form>
  </div>
</div>