// Clean definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    // Clean any pre-commit hooks in .git/hooks directory
    hooks: [".git/hooks/pre-commit"],
    builtFiles: [
      "static/build/style/*.css",
      "static/build/js/*.js",
      "static/build/images/sprites/*.png",
      "static-mobile/build/style/*.css",
      "static-mobile/build/js/*.js",
      "static-mobile/build/images/sprites/*.png"
    ],
    dist: ["dist"]
  }
};