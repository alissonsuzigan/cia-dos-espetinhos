// Copy definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    staticDesktop: {
      expand: true,
      cwd: "static/build/",
      src: [
        "fonts/**",
        "images/**",
        "js/**",
        "style/**",
        // Exlude images
        "!**/call-center/**",
        "!**/cart/**",
        // Exlude scripts
        "!**/js/vendor/**",
        "!**/js/*call-center*.js",
        "!**/js/*login*.js",
        "!**/js/*selfhelp*.js",
        "!**/js/*sign-*.js",
        // Exlude stiles
        "!**/htc/**",
        "!**/style/*call-center*.css",
        "!**/style/*selfhelp*.css",
        "!**/style/*sign-*.css",
        "!**/style/*login*.css"
      ],
      dest: "dist/static_botox/webstore/",
    },
    staticMobile: {
      expand: true,
      cwd: "static-mobile/build/",
      src: "<%= copy.staticDesktop.src %>",
      dest: "dist/static_botox/webstore-mobile/",
    },

    // Templates ---------------------------------------------------------------

    templatesDesktop: {
      expand: true,
      cwd: "template/webstore/",
      src: [
        "**",
        "!**/especial/**",
        "!**/call-center/**",
        "!**/call-center.template",
        "!**/partner*.template",
        "!**/customer-service/**",
        "!**/components/login-*.template",
        "!**/department/shelf-list/**",
        "!**/components/disclaimer-shipping.template",
        "!**/landingpage/legal-text.template"
      ],
      dest: "dist/template/webstore/",
    },
    templatesMobile: {
      expand: true,
      cwd: "template/webstore-mobile/",
      src: [
        "**",
        "!**/call-center/**",
        "!**/customer-service/**",
        "!**/department/shelf-list/**",
        "!**/components/auth-footer.template",
        "!**/components/login-*.template",
        "!**/call-center.template",
        "!**/new-password*.template",
        "!**/tag-manager.template"
      ],
      dest: "dist/template/webstore-mobile/",
    },

    // DIST to CMS -------------------------------------------------------------

    sandbox: {
      expand: true,
      cwd: "dist/",
      src: ["**"],
      dest: "/app/walmart-frontend/cms_sandbox/",
    },

    staging: {
      expand: true,
      cwd: "dist/",
      src: ["**"],
      dest: "/app/walmart-frontend/cms_staging/",
    },

    production: {
      expand: true,
      cwd: "dist/",
      src: ["**"],
      dest: "/app/walmart-frontend/cms_production/",
    },
  }
};


