// CssMin definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    default: {
      expand: true,
      cwd: "static/style/",
      src: ["*.css", "!*.min.css"],
      dest: "static/style/",
      ext: ".min.css"
    }
  }
};