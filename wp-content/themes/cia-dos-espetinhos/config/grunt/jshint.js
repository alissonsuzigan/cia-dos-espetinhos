// Jshint definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    options: options.settings.jshint,
    files: [
      "Gruntfile.js",
      "static/script/**/*.js"
    ]
  }
};