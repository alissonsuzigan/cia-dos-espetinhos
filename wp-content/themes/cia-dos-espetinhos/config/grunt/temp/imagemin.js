// ImageMin definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options"),
      defaults = {
        expand: true,
        options: {
          optimizationLevel: 4
        },
        src: [
          "**/*.{png,jpg,gif,cur}",
          "!**/call-center/**/*.{png,jpg,gif,cur}",
          "!**/cart/**/*.{png,jpg,gif,cur}"
        ]
      };

  return {

    desktop: {
      options: defaults.options,
      files: [
        {
          expand: defaults.expand,
          cwd: "static/build/images",
          src: defaults.src,
          dest: "dist/static_botox/webstore/images/",
        }
      ]
    },

    mobile: {
      options: defaults.options,
      files: [
        {
          expand: defaults.expand,
          cwd: "static-mobile/build/images",
          src: defaults.src,
          dest: "dist/static_botox/webstore-mobile/images/",
        }
      ]
    },

    spritesDesktop: {
      options: defaults.options,
      files: [
        {
          expand: defaults.expand,
          cwd: "static/build/images/sprites",
          src: defaults.src,
          dest: "dist/static_botox/webstore/images/sprites/"
        }
      ]
    },

    spritesMobile: {
      options: defaults.options,
      files: [
        {
          expand: defaults.expand,
          cwd: "static-mobile/build/images/sprites",
          src: defaults.src,
          dest: "dist/static_botox/webstore-mobile/images/sprites/"
        }
      ]
    }

  }
};